#!/bin/bash
rm -rf build
mkdir build
cd build 
cmake ..
make


cp -r ../../data/* .
cd ..
rm -rf ../data/res.xml.ox
python ../../oxygine-framework/tools/oxyresbuild.py -x res.xml --src_data ../data --dest_data  ../data
