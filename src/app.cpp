#include "oxygine-framework.h"
#include "config.h"
#include "Map.h"

using namespace oxygine;

//it is our resources
//in real project you would have more than one Resources declarations. It is important on mobile devices with limited memory and you would load/unload them


void app_preinit()
{

}
void app_init()
{
    config::init("res.xml");
    log::messageln("Hello world");
    spMap map = new Map();
    getStage()->addChild(map);

}

void app_update()
{
    config::update();
}

void app_destroy()
{
	config::free();
}



