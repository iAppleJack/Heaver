#ifndef __Cell__
#define __Cell__
#include <stdio.h>
#include <math.h>
#include "config.h"


using namespace oxygine;
using namespace std;
enum typeUnit {WALL, FLOOR, PLAYER, FLOORF, BOX, DOOR, LAVA, LUKE};

class Cell : public Actor
{
public:
    Cell(typeUnit groundCell, typeUnit currentCell, int x, int y);
    typeUnit groundCell;
    typeUnit currentCell;
    void updateCell();
    spSprite getGroundCellSprite();
    spSprite getCurrentCellSprite();

private:
    spSprite groundCellSprite;
    spSprite currentCellSprite;
    spSprite groundHelpSprite;
    Vector2 position;

};
typedef oxygine::intrusive_ptr<Cell> spCell;
#endif
