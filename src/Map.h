#ifndef __Map__
#define __Map__
#include <stdio.h>
#include <math.h>
#include "config.h"
#include "Box.h"
#include "Cell.h"
#include "Human.h"
#include "Input.h"
#include "SDL.h"
#include "SDL_keyboard.h"


using namespace oxygine;
using namespace std;


class Map : public Actor
{
public:
    Map();
    void updateMap();
    void onEvent(Event* ev);
    spHuman human;

private:
    bool collision(Vector2 vector);
    vector <vector<typeUnit>> map;
    vector <vector<spCell>> images;
    bool isWin();

    void draw();

};
typedef oxygine::intrusive_ptr<Map> spMap;
#endif
