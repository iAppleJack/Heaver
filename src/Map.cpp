#include "Map.h"
#include <iostream>
Map::Map()
{
    map = {
        {WALL,WALL,WALL,WALL,WALL,WALL,WALL,WALL,WALL},
        {WALL, FLOORF,FLOOR,FLOOR,FLOOR,FLOOR,FLOOR,FLOOR,WALL,WALL,WALL,WALL,WALL},
        {WALL,FLOOR,FLOOR,FLOOR,BOX,FLOOR,FLOOR,FLOORF,WALL,FLOOR,FLOOR,FLOOR,WALL},
        {WALL,FLOOR,LUKE,LAVA,BOX,FLOOR, FLOOR,FLOORF, WALL,FLOOR,WALL,FLOOR,WALL},
        {WALL,WALL,WALL,FLOOR,FLOOR,WALL,WALL,WALL , FLOOR, FLOOR,WALL,FLOOR,WALL},
        {WALL,WALL,WALL,FLOOR,FLOOR,WALL,WALL,WALL, BOX, FLOOR, FLOOR,FLOOR,WALL},
        {WALL,WALL,WALL,BOX,FLOOR,WALL,WALL,WALL, FLOOR , FLOOR,WALL, WALL,WALL},
        {WALL,WALL,FLOOR,FLOOR,FLOOR,FLOOR,WALL,WALL, BOX , FLOOR, FLOOR ,FLOOR, WALL, WALL, WALL},
        {WALL,FLOOR,BOX,FLOOR,FLOOR,FLOOR,FLOOR,FLOOR, FLOOR, FLOOR,BOX,FLOOR, FLOOR,FLOOR, DOOR},
        {WALL,FLOOR,FLOOR,BOX,FLOOR,BOX,FLOOR,WALL , FLOOR, FLOOR,FLOOR,WALL,WALL , WALL, WALL,WALL},
        {WALL,WALL,FLOOR,FLOOR,FLOOR,FLOOR,FLOOR,FLOOR,FLOOR,WALL,WALL,WALL},
        {WALL,WALL,WALL,WALL,WALL,WALL,WALL,WALL,WALL,WALL,WALL,WALL}
    };

    human = new Human(1,1);
    map[human->getPosition().x][human->getPosition().y] = PLAYER;

    for (int i = 0; i < map.size(); i++)
    {
        vector<spCell> line;
        images.push_back(line);
        for (int j = 0; j < map[i].size(); j++)
        {
            spCell cell;
            if (map[i][j] == FLOORF)
            {
                cell = new Cell(FLOORF, FLOORF, j*50, i*50);
            }
            else if (map[i][j] == LAVA)
            {
                cell = new Cell(LAVA, LAVA, j*50, i*50);
            }
            else
            {
                cell = new Cell(FLOOR, map[i][j], j*50, i*50);
            }

            addChild(cell);
            images[i].push_back(cell);
        }
    }
    Input::instance.addEventListener(Input::event_platform, CLOSURE(this, &Map::onEvent));
}

bool Map::isWin()
{
    for (int i = 0; i < images.size(); i++)
    {
        for (int j = 0; j < images[i].size(); j++)
        {
            if (images[i][j]->groundCell ==  FLOORF && images[i][j]->currentCell != BOX)
                return false;
        }
    }
    return true;
}

bool Map::collision(Vector2 vector)
{
    if (images[human->getPosition().x + vector.x][human->getPosition().y + vector.y]->currentCell == WALL)
    {
        return false;
    }
    else if (images[human->getPosition().x + vector.x][human->getPosition().y + vector.y]->currentCell == BOX )
    {
        if ( images[human->getPosition().x + 2*vector.x][human->getPosition().y + 2*vector.y]->currentCell == (FLOOR)  ||
             images[human->getPosition().x + 2*vector.x][human->getPosition().y + 2*vector.y]->currentCell == (FLOORF))
        {
            images[human->getPosition().x + 2*vector.x][human->getPosition().y + 2*vector.y]->currentCell = BOX;
            images[human->getPosition().x + 2*vector.x][human->getPosition().y + 2*vector.y]->updateCell();

            images[human->getPosition().x + 1*vector.x][human->getPosition().y + 1*vector.y]->currentCell =  images[human->getPosition().x + 1*vector.x][human->getPosition().y + 1*vector.y]->groundCell;

            return true;
        }
        else
        {
            return false;
        }
    }
    else if (images[human->getPosition().x + vector.x][human->getPosition().y + vector.y]->currentCell == LUKE )
    {
        if ( images[human->getPosition().x + 2*vector.x][human->getPosition().y + 2*vector.y]->currentCell == (FLOOR)  ||
             images[human->getPosition().x + 2*vector.x][human->getPosition().y + 2*vector.y]->currentCell == (FLOORF))
        {
            images[human->getPosition().x + 2*vector.x][human->getPosition().y + 2*vector.y]->currentCell = LUKE;
            images[human->getPosition().x + 2*vector.x][human->getPosition().y + 2*vector.y]->updateCell();

            images[human->getPosition().x + 1*vector.x][human->getPosition().y + 1*vector.y]->currentCell =  images[human->getPosition().x + 1*vector.x][human->getPosition().y + 1*vector.y]->groundCell;

            return true;
        }
        else if (images[human->getPosition().x + 2*vector.x][human->getPosition().y + 2*vector.y]->currentCell == (LAVA))
        {
            images[human->getPosition().x + 2*vector.x][human->getPosition().y + 2*vector.y]->currentCell = LUKE;
            images[human->getPosition().x + 2*vector.x][human->getPosition().y + 2*vector.y]->updateCell();
        }
        else
        {
            return false;
        }
    }
    else
    {
        return true;
    }

}


void Map::onEvent(Event* ev)
    {
        SDL_Event *event = (SDL_Event*)ev->userData;

        if (event->type != SDL_KEYDOWN)
            return;


        switch (event->key.keysym.sym)
        {
            case SDLK_RIGHT:
                if (collision(Vector2(0,1)))
                {
                    images[human->getPosition().x][human->getPosition().y]->currentCell =  images[human->getPosition().x][human->getPosition().y]->groundCell;
                    images[human->getPosition().x][human->getPosition().y]->updateCell();

                    human->setPosition(Vector2(0,1));
                    images[human->getPosition().x][human->getPosition().y]->currentCell =  PLAYER;
                    images[human->getPosition().x][human->getPosition().y]->updateCell();
                }
            break;
            case SDLK_LEFT:
                if (collision(Vector2(0,-1)))
                {
                    images[human->getPosition().x][human->getPosition().y]->currentCell =  images[human->getPosition().x][human->getPosition().y]->groundCell;
                    images[human->getPosition().x][human->getPosition().y]->updateCell();

                    human->setPosition(Vector2(0,-1));
                    images[human->getPosition().x][human->getPosition().y]->currentCell =  PLAYER;
                    images[human->getPosition().x][human->getPosition().y]->updateCell();
                }
            break;
            case SDLK_UP:
            if(collision(Vector2(-1,0)))
                {
                images[human->getPosition().x][human->getPosition().y]->currentCell =  images[human->getPosition().x][human->getPosition().y]->groundCell;
                images[human->getPosition().x][human->getPosition().y]->updateCell();

                human->setPosition(Vector2(-1,0));
                images[human->getPosition().x][human->getPosition().y]->currentCell =  PLAYER;
                images[human->getPosition().x][human->getPosition().y]->updateCell();
                }
            break;
            case SDLK_DOWN:
            if(collision(Vector2(1,0)))
                {
                images[human->getPosition().x][human->getPosition().y]->currentCell =  images[human->getPosition().x][human->getPosition().y]->groundCell;
                images[human->getPosition().x][human->getPosition().y]->updateCell();

                human->setPosition(Vector2(1,0));
                images[human->getPosition().x][human->getPosition().y]->currentCell =  PLAYER;
                images[human->getPosition().x][human->getPosition().y]->updateCell();
                }
            break;
        }
        for (int i = 0; i < images.size(); i++)
        {
            for (int j = 0; j < images[i].size(); j++)
            {
                log::message("%d ", images[i][j]->currentCell);
                if (isWin() && images[i][j]->currentCell == DOOR)
                {
                    ResAnim* resAnim = config::resources.getResAnim("Door");
                    spTween tween = images[i][j]->getGroundCellSprite()->addTween(TweenAnim(resAnim), 1000);
                    spTween tween2 = images[i][j]->getCurrentCellSprite()->addTween(TweenAnim(resAnim), 1000);

                }
            }
            log::messageln("");
        }
        log::messageln("");
        if (isWin())
        {

            log::messageln("Victory");
        }


    }
