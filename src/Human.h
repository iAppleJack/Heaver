#ifndef __Human__
#define __Human__
#include <stdio.h>
#include <math.h>
#include "config.h"


using namespace oxygine;
using namespace std;

class Human : public Sprite
{
public:
    Human(int x, int y);
    void printSteps();
    Vector2 getPosition();
    void setPosition(Vector2 e);

private:
    Vector2 position;
    int steps;


};
typedef oxygine::intrusive_ptr<Human> spHuman;
#endif
