#include "Human.h"
Human::Human(int x, int y)
{
    position.x = x;
    position.y = y;
    steps = 0;
}

void Human::printSteps()
{
    log::messageln("steps %d", steps);
}

Vector2 Human::getPosition()
{
    return position;
}

void Human::setPosition(Vector2 e)
{
    position.x += e.x;
    position.y += e.y;
}
