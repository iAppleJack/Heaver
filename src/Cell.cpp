#include "Cell.h"
Cell::Cell(typeUnit groundCell, typeUnit currentCell, int x, int y)
{
    this->groundCell = groundCell;
    this->currentCell = currentCell;
    position.x = x;
    position.y = y;

    groundHelpSprite = new Sprite();
    groundHelpSprite->setPosition(position);
    addChild(groundHelpSprite);


    groundCellSprite = new Sprite();
    groundCellSprite->setPosition(position);
    addChild(groundCellSprite);

    currentCellSprite = new Sprite();
    currentCellSprite->setPosition(position);
    addChild(currentCellSprite);

    updateCell();
}

spSprite Cell::getGroundCellSprite()
{
    return groundCellSprite;
}

spSprite Cell::getCurrentCellSprite()
{
    return currentCellSprite;
}

void Cell::updateCell()
{
    switch (groundCell)
    {
    case LUKE:
        groundHelpSprite->setResAnim(config::resources.getResAnim("Lava"));
        groundCellSprite->setResAnim(config::resources.getResAnim("LUKE"));
        break;
    case LAVA:
        groundCellSprite->setResAnim(config::resources.getResAnim("Lava"));
        break;
    case DOOR:
        groundCellSprite->setResAnim(config::resources.getResAnim("Floor"));
        break;
    case FLOOR:
        groundCellSprite->setResAnim(config::resources.getResAnim("Floor"));
        break;
    case FLOORF:
        groundCellSprite->setResAnim(config::resources.getResAnim("FloorF"));
        break;
    default:
        groundCellSprite->setResAnim(config::resources.getResAnim("Floor"));
        break;
    }

    switch (currentCell)
    {
    case LUKE:
        currentCellSprite->setResAnim(config::resources.getResAnim("Luke"));
        break;
    case LAVA:
        currentCellSprite->setResAnim(config::resources.getResAnim("Lava"));
        break;
    case DOOR:
        currentCellSprite->setResAnim(config::resources.getResAnim("Door"));
        break;
    case WALL:
        currentCellSprite->setResAnim(config::resources.getResAnim("Wall"));
        break;
    case BOX:
        currentCellSprite->setResAnim(config::resources.getResAnim("Box"));
        break;
    case FLOOR:
        currentCellSprite->setResAnim(config::resources.getResAnim("Floor"));
        break;
    case FLOORF:
        currentCellSprite->setResAnim(config::resources.getResAnim("FloorF"));
        break;
    case PLAYER:
        currentCellSprite->setResAnim(config::resources.getResAnim("Human"));
        break;
    }


}
